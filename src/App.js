import React, { Component } from 'react';
import './App.css';
import Category from './Category';
import ReactDOM from 'react-dom';
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';

class App extends Component 
{
    constructor(props) {
        super(props);     
            this.state = {
                Categories :[
                    'angry',
                    'bored',
                    'disappointed',
                    'drunk',
                    'embarrassed',
                    'happy',
                    'love',
                    'excited',
                    'frustrated',
                    'nervous',
                    'sad',
                    'relaxed',
                    'unimpressed',
                    'shocked',
                    'inspired',
                    'hungry',
                    'stressed',
                    'pain'
                    
                ],
                CategoriesData : {},
                ApiCount : 3,
                hasMoreItems  : true,
                requestSent : false
            }
    }
    
    componentDidMount()
    {
        window.addEventListener('scroll', this.handleOnScroll);
        this.fetchData()
    }
    
    componentWillUnmount()
    {
        window.addEventListener('scroll', this.handleOnScroll);  
    }
    
    handleOnScroll = () =>
    {
        let scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        let scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
        let clientHeight = document.documentElement.clientHeight || window.innerHeight;
        let scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;

        if (scrolledToBottom) {
          this.querySearchResult();
        }
    }
    
    fetchData = () =>  {
        let Count = 0
        for(let i = 0 ; i < this.state.ApiCount ; i++)
        {
            let category = this.state.Categories[i];
            fetch("https://api.giphy.com/v1/gifs/search?q=" + category + "&api_key=iSBbKlfRyUIn2w669pKqCpt9yrbNaaqj&limit=25")
            .then(results => {
                return results.json()
            })
            .then( data => {
                this.state.CategoriesData[category] = data;
                Count++;
                if(Count == this.state.ApiCount)
                {
                    if(this.state.ApiCount == this.state.Categories.length)
                    {
                        this.setState({
                            hasMoreItems: false,
                        });
                    }
                    
                    Count = this.state.ApiCount + 3
                    this.setState({
                            ApiCount: Count,
                            requestSent: false
                    });
                }
               // ReactDOM.render(<Category Category = {category} CategoryData = {data.data}/> , element)
            })
        }
    }
    
    slideLeft = (Category , Count) => {

    }
    
    slideRight = (Category , Count) => {

    }
    
    generateDom = () =>
    {
        let elements = []
        for(let i = 0 ; i < this.state.Categories.length ; i++)
        {
               let category = this.state.Categories[i] 
               let data = this.state.CategoriesData[category];
               if(data !== undefined)
               {
                    elements.push(<Category Category = {category} CategoryData = {data.data} 
                                    slideLeft = {this.slideLeft} slideRight = {this.props.slideRight}/>)
               } 
         }
                                  
        return elements;
    }
                  
    componentWillMount()
    {
        let element = document.getElementById("root")
        let height = window.innerHeight - 5
        let width = window.innerWidth - 17
        element.style.height = height +"px"
        element.style.width = width + "px"
    }
                   
    querySearchResult = () => {
        if (this.state.requestSent || !(this.state.hasMoreItems)) {
          return;
        }
        this.setState({
            requestSent: true
        });
        
        setTimeout(this.fetchData, 2000);
    }
    
    render() 
    {  
    return (
      <div className = "OuterApp">
        <div className="App">
            {this.generateDom()}
        </div>
          {(() => {
          if (this.state.requestSent) {
            return(
              <div className="data-loading">
                <CircularProgress className = "spinner" color="inherit" />
              </div>
            );
          } else {
            return(
              <div className="data-loading"></div>
            );
          }
        })()}
    </div>
    );
  }
}

export default App;
