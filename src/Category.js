import React, { Component } from 'react';
import './App.css';
import GiphDiv from './GiphDiv'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class Category extends Component 
{   
    generateDom = () => 
    {       
        let elements = [];
        for(let i = 0 ; i < this.props.CategoryData.length ; i++)
        {
            let Data = this.props.CategoryData[i];
            elements.push(<div className = "category_image_div">
                            <img src = {Data.images.fixed_height.url} className = "image_div"/>
                         </div>
                        )
        }
        
        return elements
    }
    
    slideLeft = () => {
    }
    
    slideRight = () => {
    }

    render() {
    let elementidName = "Outer_" + this.props.Category;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5
    };    
    let CategoryName = this.props.Category.charAt(0).toUpperCase() + this.props.Category.slice(1);
    return (
      <div className="sub_category_main_outer_div">
        <div className="sub_category_header_div">
            {CategoryName}
        </div>
        <div className="sub_category_main_div" id = {elementidName}>
            <Slider {...settings}>
                {this.generateDom()}
            </Slider>
        </div>
      </div>
    );
  }
}

export default Category;
